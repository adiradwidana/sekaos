$(document).ready(function() {

    // Attach fastclick
    FastClick.attach(document.body);

    // Activate tooltip bootstrap
    $('[data-toggle=tooltip]').tooltip({
        container: 'body',
    });



    // Add your own custom script
    // ...
    if($('header').hasClass('navbar-inverse')) {

        var bodyScrolled = window.scrollY >= 160;

        $(document).on('scroll', function() {
            if (bodyScrolled && window.scrollY < 160 || !bodyScrolled && window.scrollY >= 160) {
                toggleNavbar();
            }
        })

        var toggleNavbar = function() {
            bodyScrolled = !bodyScrolled;

            if (bodyScrolled) {
                $('header').removeClass('navbar-inverse').addClass('navbar-fixed-top navbar-default');
            } else {
                $('header').addClass('navbar-inverse').removeClass('navbar-default');
            }
        }
    }

});
