<?php namespace Sekaos\Commerce;

use Event;
use Yaml;
use File;
use Currency;
use Backend;
use Illuminate\Foundation\AliasLoader;
use Rainlab\User\Models;
use Rainlab\User\Models\User as User;
use System\Classes\PluginBase;

/**
 * Commerce Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Commerce',
            'description' => 'No description provided yet...',
            'author'      => 'Sekaos',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        $alias = AliasLoader::getInstance();
        $alias->alias('Cart', 'Sekaos\Commerce\Facades\Cart');
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        User::extend(function($model){

            $model->addFillable([
                'phone',
                'address',
                'state_id',
                'country_id',
                'city_id',
                'postcode'
            ]);

            $model->hasMany['ratings'] = ['Sekaos\Commerce\Models\Rating'];
            $model->hasMany['carts'] = ['Sekaos\Commerce\Models\Cart'];
            $model->hasMany['products'] = ['Sekaos\Commerce\Models\product'];
            $model->hasMany['orders'] = ['Sekaos\Commerce\Models\Order'];
            $model->hasMany['wishlists'] = ['Sekaos\Commerce\Models\Wishlist'];

            $model->belongsTo['city'] = ['Sekaos\Commerce\Models\City'];
            $model->belongsTo['state'] = ['Rainlab\Location\Models\State'];

        });
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {

        return [
            'Sekaos\Commerce\Components\Cart'          => 'cart',
            'Sekaos\Commerce\Components\Wishlist'      => 'wishlist',
            'Sekaos\Commerce\Components\ProductList'   => 'productList',
            'Sekaos\Commerce\Components\ProductDetail' => 'productDetail',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'sekaos.commerce.some_permission' => [
                'tab' => 'Commerce',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'commerce' => [
                'label'       => 'Commerce',
                'url'         => Backend::url('sekaos/commerce/products'),
                'icon'        => 'icon-leaf',
                'permissions' => ['sekaos.commerce.*'],
                'order'       => 500,
            'sideMenu' => [
                'products' => [
                      'label'       => 'Product',
                      'icon'        => 'icon-file-text-o',
                      'url'         => Backend::url('sekaos/commerce/products'),
                      'permissions' => ['sekaos.commerce.*'],
                      'order'       => 500,
                ],
                'categories' => [
                      'label'       => 'Categories',
                      'icon'        => 'icon-file-text-o',
                      'url'         => Backend::url('sekaos/commerce/categories'),
                      'permissions' => ['sekaos.commerce.*'],
                      'order'       => 500,
                ]
              ]
            ],
        ];
    }
}
