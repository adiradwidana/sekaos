<?php namespace Sekaos\Commerce\Models;

use Model;

/**
 * City Model
 */
class City extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'sekaos_commerce_cities';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    public $implement = ['Sekaos.Commerce.Behaviors.CityModel'];
    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'state' => 'Rainlab\Location\Models\State'
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
