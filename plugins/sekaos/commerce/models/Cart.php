<?php namespace Sekaos\Commerce\Models;

use Model;

/**
 * Cart Model
 */
class Cart extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'sekaos_commerce_carts';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['session_id'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
      'user' => 'Rainlab\User\Model\User',
    ];
    public $belongsToMany = [
      'products' => [
        'Sekaos\Commerce\Models\Product',
        'table' => 'sekaos_commerce_cart_product',
        'pivot' => ['quantity', 'total'] 
      ]
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
