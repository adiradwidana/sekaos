<?php namespace Sekaos\Commerce\Models;

use Model;

/**
 * Product Model
 */
class Product extends Model
{
    use \October\Rain\Database\Traits\Sluggable;
    use \Sekaos\Commerce\Traits\Filterable;
    /**
     * @var string The database table used by the model.
     */
    public $table = 'sekaos_commerce_products';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Generate slugs for these attributes.
     */
    protected $slugs = ['slug' => 'name'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'ratings' => 'Sekaos\Commerce\Models\Rating'
    ];
    public $belongsTo = [
        'category' => 'Sekaos\Commerce\Models\Category',
        'user' => 'Rainlab\User\Models\User'
    ];
    public $belongsToMany = [
        'colors' => [
            'Sekaos\Commerce\Models\Color', 
            'table' => 'sekaos_commerce_product_color'
            ],
        'vendors' => [
            'Sekaos\Commerce\Models\Vendor', 
            'table' => 'sekaos_commerce_product_vendor'
            ],
        'products' => [
            'Sekaos\Commerce\Models\Cart',
            'table' => 'sekaos_commerce_cart_product',
            'pivot' => ['quantity', 'total'] 
            ]
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
      'front_image' => 'System\Models\File',
      'back_image' => 'System\Models\File'
    ];
    public $attachMany = [];
}
