<?php namespace Sekaos\Commerce\Models;

use Model;

/**
 * Rating Model
 */
class Rating extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'sekaos_commerce_ratings';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'user' => 'Rainlab\User\Models\User',
        'product' => 'Sekaos\Commerce\Models\Product'
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
