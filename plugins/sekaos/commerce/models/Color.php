<?php namespace Sekaos\Commerce\Models;

use Model;

/**
 * Color Model
 */
class Color extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'sekaos_commerce_colors';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [
        'product_color' => ['Sekaos\Commerce\Models\Color', 'table' => 'sekaos_commerce_product_color']
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
