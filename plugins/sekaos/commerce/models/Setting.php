<?php namespace Sekaos\Commerce\Models;

use Model;

/**
 * Setting Model
 */
class Setting extends Model
{
    /**
     * @var string The database table used by the model.
     */
    use \October\Rain\Database\Traits\Validation;

    public $implement = ['System.Behaviors.SettingsModel'];

    public $settingsCode = 'sekaos_commerce_settings';

    public $settingsFields = 'fields.yaml';

    /**
     * Validation rules
     */
    public $rules = [];

    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'base_location' => 'Sekaos\Commerce\Models\City',
        'default_currency' => 'Sekaos\Commerce\Models\Currency',
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];


    public function getProductsPageOptions() {
        return Page::getNameList();
    }

    public function getCategoryPageOptions() {
        return Page::getNameList();
    }

    public function getProductDetailPageOptions() {
        return Page::getNameList();
    }

    public function getCartPageOptions() {
        return Page::getNameList();
    }

    public function getCheckoutPageOptions() {
        return Page::getNameList();
    }
}
