<?php namespace Sekaos\Commerce\Models;

use Model;

/**
 * Size Model
 */
class Size extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'sekaos_commerce_sizes';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'orders' => 'Sekaos\Commerce\Models\Order'
    ];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
