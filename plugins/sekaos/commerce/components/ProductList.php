<?php namespace Sekaos\Commerce\Components;

use DB;
use Input;
use Request;
use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use Sekaos\Commerce\Models\Product;
use Sekaos\Commerce\Models\Category;

class ProductList extends ComponentBase
{
    public $products;
    public $currentPage;
    public $categorySlug;
    public $category;
    public $categoryslug;


    public function componentDetails()
    {

        return [
            'name'        => 'List Product',
            'description' => ''
        ];
    }

    public function defineProperties()
    {
        return [
            'categorySlug' => [
                'title'       => 'octommerce.octommerce::lang.component.product_list.param.category_param_title',
                'description' => 'octommerce.octommerce::lang.component.product_list.param.category_param_desc',
                'default'     => '',
                'type'        => 'string'
            ],
            'categoryFilter' => [
                'title'       => 'octommerce.octommerce::lang.component.product_list.param.categoryfilter_param_title',
                'description' => 'octommerce.octommerce::lang.component.product_list.param.categoryfilter_param_desc',
                'type'        => 'dropdown',
                'default'     => '',
                'group'       => 'Filter',
            ],
        ];
    }

    public function onRun()
    {
        $categorySlug;
        $currentPage = post('page');

        $this->page['categories'] = $this->category = $this->listCategories();
        $products = $this->page['products'] = $this->listProducts();
    }

    public function listProducts()
    {
        $query = Product::with(['category','colors', 'user'])->orderBy('created_at','desc');
        
        if($this->property('categoryFilter') != '') {
            $this->filterByCategory($query, $this->property('categoryFilter'));
        }

        return $query->get();
    }

    public function listCategories()
    {
        $categories = Category::all();

        return $categories;
    }

    public function filterByCategory(&$query, $slug)
    {
        $category = $this->category = Category::whereSlug($slug)->first();
        if ($category) {
            $query->whereHas('category', function($q) use($category){
                $q->whereId($category->id);
            });
        }
        else{
            return 'No Product Found';
        }
    }

    public function onChangeCategory()
    {   
        $categoryslug = post('category_slug');

        $products = $this->page['products'] = Product::with('category','colors','user')
            ->whereHas('category', function($query) use($categoryslug){
                $query->whereSlug($categoryslug);})
            ->orderBy('created_at','desc')
            ->get();
    }
}
