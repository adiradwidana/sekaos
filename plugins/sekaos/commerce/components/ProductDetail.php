<?php namespace Sekaos\Commerce\Components;

use Cms\Classes\ComponentBase;
use Sekaos\Commerce\Models\Product;

class ProductDetail extends ComponentBase
{
    public $product;

    public function componentDetails()
    {
        return [
            'name'        => 'ProductDetail Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'title' => 'Slug',
                'description' => 'Description',
                'default' => '{{ :slug }}',
                'type' => 'string'
            ]
        ];
    }

    public function onRun(){
        $product = $this->loadProduct();

        if (!$product) {
            $this->setStatusCode(404);
            return $this->controller->run('404');
        }

        $this->product = $this->page['product'] = $product;

        $this->page->title = $product->name;
        $this->page->description = $product->description;

    }

    public function loadProduct(){
        $slug = $this->property('slug');

        $product = Product::whereSlug($slug)
            ->with('category')
            ->first();
            
        return $product;
    }
}
