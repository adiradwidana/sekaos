<?php namespace Sekaos\Commerce\Components;

use Auth;
use Flash;
use Cms\Classes\ComponentBase;
use Sekaos\Commerce\Models\Product;
use Sekaos\Commerce\Models\Wishlist as WishlistModel;

class Wishlist extends ComponentBase
{
    public $wishlist;
    public $wishlistManager;

    public function componentDetails()
    {
        return [
            'name'        => 'Wishlist',
            'description' => 'Add your product to your wishlist'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
         $this->wishlist = $this->page['wishlist'] = Auth::check() ? $this->getUserWishlist(Auth::getUser()->id) : [];
    }

    public function onAdd()
    {
        //dd(post('product_id'));
        if (Auth::check()) {
            $product = Product::where('id', post('product_id'))->first();

            try {
                $wishlist             = new WishlistModel;
                $wishlist->product_id = $product->id;
                $wishlist->user_id    = Auth::getUser()->id;
                $wishlist->save();
            }
            catch (Exception $e){
                return new \ApplicationException($e->getMessage());
            }

            Flash::success($product->name. ' has been added to your wishlist.');
            return $product->name. ' has been added to your wishlist';
        }
        else {
            throw new \ApplicationException('Sorry, you should logged in first');
        }
    }
    public function onRemove()
    {
        if(Auth::check()) {
            $wishlist = WishListModel::find(post('wishlist_id'));

		    $wishlist->delete();	

            Flash::success('Wishlist has been removed');
            return 'Wishlist has been removed.';
       }
    }

    public function getUserWishlist($user_id)
    {
        $userWishlist = WishListModel::whereUserId($user_id)->with('product', 'user')->get();

		return $userWishlist;
    }
}
