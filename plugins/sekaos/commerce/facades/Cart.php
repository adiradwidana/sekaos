<?php namespace Sekaos\Commerce\Facades;

use October\Rain\Support\Facade;

class Cart extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'sekaos.commerce.helper';
    }

    protected static function getFacadeInstance()
    {
        return new \Sekaos\Commerce\Helpers\Cart;
    }
}
