<?php namespace Sekaos\Commerce\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCartsTable extends Migration
{
    public function up()
    {
        Schema::create('sekaos_commerce_carts', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('session_id');
            $table->integer('user_id');

            $table->timestamps();
        });

        Schema::create('sekaos_commerce_cart_product', function(Blueprint $table){
            $table->engine = 'InnoDB';
            $table->integer('cart_id');
            $table->integer('product_id');

            $table->integer('quantity')->default(0);
            $table->decimal('total', 12, 2)->default(0);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('sekaos_commerce_carts');
        Schema::dropIfExists('sekaos_commerce_cart_product');
    }
}
