<?php namespace Sekaos\Commerce\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSizesTable extends Migration
{
    public function up()
    {
        Schema::create('sekaos_commerce_sizes', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('size');
            $table->decimal('width', 12, 2)->default(0);
            $table->decimal('height', 12, 2)->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('sekaos_commerce_sizes');
    }
}
