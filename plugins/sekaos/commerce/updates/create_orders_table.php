<?php namespace Sekaos\Commerce\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateOrdersTable extends Migration
{
    public function up()
    {
        Schema::create('sekaos_commerce_orders', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('order_no');
            $table->integer('user_id');

            $table->string('currency')->default('IDR');
            $table->decimal('subtotal', 12, 2)->unsigned()->default(0);

            $table->text('notes')->nullable();
            $table->string('status_code')->nullable();
            $table->timestamp('status_updated_at');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('sekaos_commerce_orders');
    }
}
