<?php namespace Sekaos\Commerce\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateOrderProductsTable extends Migration
{
    public function up()
    {
        Schema::create('sekaos_commerce_order_product', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('order_id');
            $table->integer('product_id');
            $table->integer('qty');
            $table->integer('package_id');
            $table->integer('size_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('sekaos_commerce_order_product');
    }
}
