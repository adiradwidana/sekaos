<?php namespace Sekaos\Commerce\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProductsTable extends Migration
{
    public function up()
    {
        Schema::create('sekaos_commerce_products', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('category_id');
            $table->integer('product_id');
            $table->integer('user_id');

            $table->string('name');
            $table->string('slug');
            $table->string('keywords');
            $table->decimal('price', 12, 2)->default('0');
            $table->boolean('featured');
            $table->integer('stocks');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('sekaos_commerce_products');
    }
}
