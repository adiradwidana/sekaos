<?php namespace Sekaos\Commerce\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateWishlistsTable extends Migration
{
    public function up()
    {
        Schema::create('sekaos_commerce_wishlists', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('sekaos_commerce_wishlists');
    }
}
