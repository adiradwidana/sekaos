<?php namespace Sekaos\Commerce\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateColorsTable extends Migration
{
    public function up()
    {
        Schema::create('sekaos_commerce_colors', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('hex');
            $table->timestamps();
        });

        Schema::create('sekaos_commerce_product_color', function (Blueprint $table){
            $table->engine = 'InnoDB' ;
            $table->integer('product_id');
            $table->integer('color_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('sekaos_commerce_colors');
        Schema::dropIfExists('sekaos_commerce_product_color');
    }
}
