<?php namespace Sekaos\Commerce\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePackagesTable extends Migration
{
    public function up()
    {
        Schema::create('sekaos_commerce_packages', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->integer('min_qty');
            $table->integer('max_qty');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('sekaos_commerce_packages');
    }
}
