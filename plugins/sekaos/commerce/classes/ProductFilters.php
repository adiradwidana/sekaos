<?php namespace Sekaos\Commerce\Classes;

use Illuminate\Database\Eloquent\Builder;
use Sekaos\Commerce\Contracts\QueryFilter;

/**
 * 
 */
class ProductFilters extends QueryFilters
{
    // create filter here
    /**
     * Filter by popularity.
     *
     * @param  string $order
     * @return Builder
     */
     public function category($slug = null)
     {
        return $this->builder->whereHas('categories', function($q) use($slug){
            $q->whereSlug($slug);
        });
     }
}
?>