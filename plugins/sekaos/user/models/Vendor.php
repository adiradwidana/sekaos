<?php namespace Sekaos\User\Models;

use Model;

/**
 * Vendor Model
 */
class Vendor extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'sekaos_user_vendors';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [
        'products' => [
            'Sekaos\Commerce\Models\Product', 'table' => 'sekaos_commerce_product_vendor'
        ]
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
