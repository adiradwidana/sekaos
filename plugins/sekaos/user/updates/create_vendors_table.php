<?php namespace Sekaos\User\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateVendorsTable extends Migration
{
    public function up()
    {
        Schema::create('sekaos_user_vendors', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');

            $table->string('vendor_name');
            $table->string('address');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('sekaos_user_vendors');
    }
}
