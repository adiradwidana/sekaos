<?php namespace Sekaos\User\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProductVendorTable extends Migration
{
    public function up()
    {
        Schema::create('sekaos_commerce_product_vendor', function(Blueprint $table){
            $table->engine = 'InnoDB';
            $table->integer('product_id');
            $table->integer('vendor_id');
        });


    }

    public function down()
    {
        Schema::dropIfExists('sekaos_commerce_product_vendor');
    }
}
